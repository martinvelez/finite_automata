# This class defines the finite_automata Ruby library.
class FiniteAutomata
	attr_accessor :states, :start_state, :alphabet, :state_transitions

	def initialize(states = [], start_state = nil, alphabet = [], 
		state_transitions = {})
		@states = states
		@start_state = start_state
		@alphabet = alphabet
		@state_transitions = state_transitions
	end
end


class FST < FiniteAutomata
	attr_accessor :output_alphabet, :output_function

	def initialize(states = [], start_state = nil, alphabet = [], 
		state_transitions = {}, output_alphabet = [], output_function = [])
		super(states, start_state, alphabet, state_transitions)
		@output_alphabet = output_alphabet
		@output_function = output_function
	end

end


# A deterministic finite automaton M is a 5-tuple (Q, Σ, δ, q0, F), 
# consisting of
# * Q, a finite set of states
# * Σ, a finite set of input symbols called the alphabet
# * δ, a state transition function (δ: Q x Σ--> Q)
# * q0, a start state (q0 ∈ Q)
# * F, a set of accept states (F ⊆ Q)
#
class DFA < FiniteAutomata
	attr_accessor :accepting_states

	def initialize(states = [], start_state = nil, alphabet = [], 
		state_transitions = {}, accepting_states = []) 
		super(states, start_state, alphabet, state_transitions)
		@accepting_states = accepting_states
	end

	# Let M = (Q, Σ, δ, q0, F) be a finite automaton 
	# and let w = w1w2...wn be a string where each w_i is a member of the 
	# alphabet Σ.  Then M <b>accepts</b> w if a sequence of states 
	# r_0, r_1, ..., r_n in Q exists with three conditions:
	# 1. r_0 = q_0
	# 2. δ(r_i, w_(i+1)) = r_(i+1) for i = 0, ..., n-1 and
	# 3. r_n ∈ F
	def run(word)
		states = [@start_state]
		word.each_char do |c| 
			states.push(@state_transitions[states.last][c]) 
		end
		return @accepting_states.include?(states.last)
	end

end


# A Moore machine M is a 6-tuple (Q, Σ, Γ, δ, ω, q0)
# consisting of 
# * Q, a finite set of states
# * Σ, a finite set of input symbols
# * Γ, a finite set of ouput symbols
# * δ, a state transition function (δ: Q x Σ --> Q)
# * ω, an output function (ω: Q --> Γ)
# * q0, a start state (q0 ∈ Q) 
#
# Moore machines differ from Mealy machines in the output function.
# 
class MooreMachine < FST
	def run(word)
		states = [@start_state]
		output = @output_function[@start_state]
		word.each_char do |c|
			states.push(@state_transitions[states.last][c])
			output << @output_function[states.last]
		end		
		return output
	end
end


# A Mealy machine M is a 6-tuple (Q, Σ, Γ, δ, ω, q0)
# consisting of 
# * Q, a finite set of states
# * Σ, a finite set of input symbols
# * Γ, a finite set of ouput symbols
# * δ, a state transition function (δ: Q x Σ --> Q)
# * ω, an output function (ω: Q x Σ --> Γ)
# * q0, a start state (q0 ∈ Q) 
#
# Mealy machines differ from Moore machines in the output function.
#
class MealyMachine < FST

	def run(word)
		output = ""
		word.each_char do |c|
			states.push(@state_transitions[states.last][c])
			output << @output_function[states.last][c]
		end 
		return output
	end
end

