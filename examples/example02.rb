#!/usr/bin/env ruby

require '../lib/finite_automata.rb'

m = MooreMachine.new
m.states = [:q0,:q1,:q2]
m.alphabet = [0,1]
m.state_transitions = {:q0 => {0 => :q1, 1 => :q2}, 
 :q1 => {0 => :q3, 1 => :q2}, 
 :q2 => {0 => :q2, 1 => :q2} }

m.output_function = {:q0 => "", :q1 => 0, :q2 => 1}
m.start_state = :q0


word = ARGF.read.chomp
puts word.inspect
puts m.inspect

puts m.run(word)
