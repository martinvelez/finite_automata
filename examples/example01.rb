#!/usr/bin/env ruby

require '../lib/finite_automata.rb'

dfa = DFA.new
dfa.states = [:q1,:q2,:q3]
dfa.alphabet = [0,1]
dfa.state_transitions = {:q1 => {0 => :q1, 1=>:q2}, 
 :q2 => {0 => :q3, 1 => :q2}, 
 :q3 => {0 => :q2, 1 => :q2} }
dfa.start_state = :q1
dfa.accepting_states = [:q2]


word = ARGF.read.chomp
puts word.inspect
puts dfa.inspect

puts dfa.run(word)
