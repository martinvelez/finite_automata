#!/usr/bin/env ruby

require '../lib/finite_automata.rb'

m = MealyMachine.new
m.states = [:s0,:s1,:s2]
m.alphabet = [0,1]

m.state_transitions = {:q1 => {1 => :q1, 2 => :q2}, 
 :q2 => {0 => :q3, 1 => :q2}, 
 :q3 => {0 => :q2, 1 => :q2} }
m.start_state = :q1


word = ARGF.read.chomp
puts word.inspect
puts m.inspect

puts m.run(word)
