Gem::Specification.new do |s|
	s.name = 'finite_automata'
	s.version = '0.0.1'
	s.date = '2012-03-27'
	s.summary = "A finite automata library"
	s.description = "A library for describing and visualizing finite automata"
	s.authors = ["Martin Velez"]
	s.email = 'mvelez999@gmail.com'
	s.files = Dir["{lib,examples}/**/*"] + ["README.rdoc","LICENSE"]
	s.homepage = 'https://bitbucket.org/martinvelez/finite_automata'
	s.require_paths = ["lib"]
end
